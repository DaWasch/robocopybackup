REM files from folder A to B 
REM switch /E active (no deletions in destination), Use /MIR instead of /E for complete sync (including deletions)
robocopy C:\A\ D:\B\ /E

REM copy Folder to remote SAMBA share. Use Login for SAMBA Server
REM switch /E active (no deletions in destination), Use /MIR instead of /E for complete sync (including deletions)
REM switch /FTT used to cope with time differences between source and destination up to 2 seconds
net use \\<IP-of-share>\<ExposedFolder> /user:<USER> <PASSWORD>
robocopy C:\A\ \\<IP-of-share>\<ExposedFolder>\<DestinationFolder>\ /E /FFT
net use \\<IP-of-share>\<ExposedFolder> /DEL